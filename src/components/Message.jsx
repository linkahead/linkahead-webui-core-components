import React, { useState } from "react";
import PropTypes from "prop-types";
import Alert from "react-bootstrap/Alert";

export const Message = ({
  heading,
  icon,
  type,
  dismissible,
  children,
  className,
}) => {
  var variant = "primary";
  const showIcon = icon === undefined || icon;
  var iconClass = "bi bi-exclamation-circle";
  var [show, setShow] = useState(true);

  if (!show) {
    return <React.Fragment />;
  }

  if (type === "error") {
    variant = "danger";
    iconClass = "bi bi-exclamation-triangle-fill";
  } else if (type === "warning") {
    variant = "warning";
    iconClass = "bi bi-exclamation-triangle";
  } else if (type === "success") {
    variant = "success";
    iconClass = "bi bi-check-circle";
  }

  icon = showIcon && (icon || <i className={`${iconClass} me-2 m-0`}></i>);

  return (
    <Alert
      className={className}
      variant={variant}
      onClose={() => setShow(false)}
      dismissible={dismissible}
    >
      {heading && (
        <Alert.Heading>
          {icon}
          {heading}
        </Alert.Heading>
      )}
      <div className="d-flex align-items-center m-0">
        {!heading && icon}
        <div>{children}</div>
      </div>
    </Alert>
  );
};

Message.propTypes = {
  heading: PropTypes.node,
  icon: PropTypes.node,
  type: PropTypes.oneOf(["error", "warning", "success", "info"]),
  dismissible: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  className: PropTypes.string,
};

export const ErrorMessage = ({ heading, error }) => {
  console.warn(error);
  heading = heading || "Error";

  var message;
  if (typeof error === "string" || error instanceof String) {
    message = error;
  } else if (
    error.message &&
    (typeof error.message === "string" || error.message instanceof String)
  ) {
    message = error.message;
  } else {
    message = JSON.stringify(error);
  }

  return (
    <Message className="mt-3 mx-3" heading={heading} type="error" dismissible>
      {message}
    </Message>
  );
};

ErrorMessage.propTypes = {
  heading: PropTypes.node,
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Error)]),
};
