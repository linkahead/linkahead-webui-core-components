import React, { useState } from "react";
import { InfoService } from "../InfoService";

function handleError(error) {
  return "Could not retrieve Server Version Info: " + error.message;
}

function getServerVersionInfo(setVersion) {
  const service = new InfoService();
  service
    .getVersionInfo()
    .then((serverVersion) => serverVersion.noBuild().toString())
    .catch(handleError)
    .then(setVersion);
  return "..."; // some nicer waiting info?
}

export function ServerVersionInfo() {
  var [version, setVersion] = useState(null);
  if (version === null) {
    version = getServerVersionInfo(setVersion);
  }

  return <div className="caosdb-comp-server-version-info">{version}</div>;
}
