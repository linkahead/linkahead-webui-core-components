"use strict";

import React from "react";
import ReactDOM from "react-dom/client";
import "../src/i18n";
import "bootstrap/scss/bootstrap.scss";
import "bootstrap-icons/font/bootstrap-icons.css";

import { Header } from "../src/components/Header";
import { Footer } from "../src/components/Footer";
import { Card } from "../src/components/Card";
import "../src/index.scss";

function setup() {
  const header = document.querySelector("#header");
  const card = document.querySelector("#card");
  const footer = document.querySelector("#footer");

  const headerRoot = ReactDOM.createRoot(header);
  headerRoot.render(React.createElement(Header));

  const cardRoot = ReactDOM.createRoot(card);
  cardRoot.render(
    React.createElement(
      Card,
      {},
      React.createElement(Card.Header, {
        key: "header",
        title: "This is a Card.Header",
      }),
      React.createElement(
        Card.Body,
        {
          key: "body",
        },
        "This is a Card.Body",
      ),
      React.createElement(
        Card.Footer,
        {
          key: "footer",
        },
        "This is a Card.Footer",
      ),
    ),
  );

  const footerRoot = ReactDOM.createRoot(footer);
  footerRoot.render(React.createElement(Footer));
}

setup();
