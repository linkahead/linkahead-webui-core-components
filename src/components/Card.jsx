import React from "react";
import PropTypes from "prop-types";

const Card = function ({ className, children }) {
  className = className || "";

  return (
    <div className={`card mt-3 rounded shadow ${className}`}>{children}</div>
  );
};

Card.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

Card.Body = function CardBody({ children }) {
  return <div className="card-body">{children}</div>;
};

Card.Body.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

Card.Footer = function CardFooter({ children }) {
  return (
    <div className="card-footer d-flex justify-content-between">{children}</div>
  );
};

Card.Footer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

Card.Title = function CardTitle({ children }) {
  return <h3 className="card-title">{children}</h3>;
};

Card.Title.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

Card.Header = function CardHeader({ title, children }) {
  title = title ? <Card.Title>{title}</Card.Title> : undefined;
  return (
    <div className="card-header">
      {title}
      {children}
    </div>
  );
};

Card.Header.propTypes = {
  title: PropTypes.node,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export { Card };
