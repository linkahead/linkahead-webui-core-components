const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = function(env, argv) {
const config = {
  entry: path.resolve(__dirname, './src/index.js'),
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.css$/i,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
    alias: {
      "react": path.resolve(__dirname, './node_modules/react')
    }
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'caosdb-webui-core-components.js',
    library: 'CaosDBCoreComponents',
    libraryTarget: 'umd',
  },
  externals: {
    react: {
      commonjs: "react",
      commonjs2: "react",
      amd: "React",
      root: "React"
    },
    reactDom: {
      commonjs: "react-dom",
      commonjs2: "react-dom",
      amd: "ReactDom",
      root: "ReactDom"
    },
    "@indiscale/caosdb-webui-info-service": {
      commonjs: "@indiscale/caosdb-webui-info-service",
      commonjs2: "@indiscale/caosdb-webui-info-service",
      amd: "CaosDBInfoService",
      root: "CaosDBInfoService"
    },
  },
};

if(env["include-peer-deps"]) {
  // for development
  config.externals = {}
  config.entry = {
    index: path.resolve(__dirname, './dev/index.js'),
    page: path.resolve(__dirname, './dev/page.js'),
  }
  config.output = {
    path: path.resolve(__dirname, './dist'),
    filename: 'caosdb-webui-core-components.[name].js',
    library: 'CaosDBCoreComponents',
    libraryTarget: 'umd',
  }
  config.plugins = [
    new HTMLWebpackPlugin({
      favicon: false,
      showErrors: true,
      cache: true,
      chunks: [ "index" ],
      template: path.resolve(__dirname, 'dev/index.html')
    }),
    new HTMLWebpackPlugin({
      filename: "page.html",
      favicon: false,
      showErrors: true,
      cache: true,
      chunks: [ "page" ],
      template: path.resolve(__dirname, 'dev/page.html')
    })
  ]
  config.devServer = {
    port: 8083,
    static: [
      path.resolve(__dirname, "./dist"),
      path.resolve(__dirname, "./static"),
    ]
  }
}


return config;
}
