/* eslint-disable react/prop-types */
import React, {
  useRef,
  useMemo,
  useEffect,
  useState,
  useCallback,
} from "react";
import { Dropzone, FileMosaic } from "@indiscale/files-ui-react";
import { Message } from "./Message";
import { Await } from "./Await";
import { LoadingInfo } from "./LoadingInfo";

/**
 * Represents a single new directory (folder).
 *
 * Component is editable when the user clicks on it and changes back to "text"
 * when they are done.
 */
const SingleNewDir = ({ name, edit, setNewDir, idx }) => {
  const ref = useRef();
  const [_name, setName] = useState(name || "");

  useEffect(() => {
    if (edit) {
      ref.current.focus();
    }
  }, [ref, edit]);

  useEffect(() => {
    if (_name.indexOf("/") > -1) {
      const dirs = _name.split("/", 2);
      setName(dirs[0]);
      const add = { name: dirs[1], edit: true };
      setNewDir({ name: dirs[0], edit: false }, idx, add);
    }
  }, [_name, setNewDir, idx]);

  if (!edit) {
    const onClick = () => {
      setNewDir({ name: _name, edit: true }, idx);
    };
    return (
      <button
        ref={ref}
        onClick={onClick}
        className="btn btn-link"
        style={{ paddingLeft: "0.1rem", paddingRight: "0rem" }}
      >
        {_name}/
      </button>
    );
  }

  const onChange = (e) => {
    setName(e.target.value);
  };

  const onBlur = (e) => {
    if (e.relatedTarget?.classList.contains("caosdb-f-cancel-new-directory")) {
      setNewDir({ edit: false }, idx);
    } else {
      setNewDir({ name: _name, edit: false }, idx);
    }
  };
  const onKeyDown = (e) => {
    if (e.key === "Enter") {
      setNewDir({ name: _name, edit: false }, idx);
    }
  };
  return (
    <span className="input-group">
      <input
        ref={ref}
        onChange={onChange}
        value={_name}
        onKeyDown={onKeyDown}
        className="form-control"
        title="Insert new directory name. A directory name must not contain the slash '/'."
        onBlur={onBlur}
      />
      <button className="btn btn-outline-secondary caosdb-f-cancel-new-directory">
        <i className="bi bi-x" />
      </button>
      <button className="btn btn-outline-secondary">
        <i className="bi bi-check" />
      </button>
    </span>
  );
};

/**
 * Represents all directories beneath the directoryBase.
 */
const NewDirs = ({ setDirectory, value }) => {
  const [newDirs, setNewDirs] = useState(
    value.map((d) => {
      return { name: d };
    }),
  );

  const setSingleNewDir = useCallback(
    (d, idx, add) => {
      var _newDirs = [].concat(newDirs);
      if (d.name) {
        _newDirs[idx] = d;
      } else {
        _newDirs = _newDirs.slice(0, idx).concat(_newDirs.slice(idx + 1));
      }
      if (add) {
        _newDirs = _newDirs
          .slice(0, idx + 1)
          .concat([add], _newDirs.slice(idx + 1));
      }
      setNewDirs(_newDirs);
      setDirectory(_newDirs.map((d) => d.name));
    },
    [newDirs, setNewDirs, setDirectory],
  );

  const addDir = useCallback(() => {
    const _newDirs = newDirs.map((d) => {
      return { ...d, edit: false };
    });
    _newDirs.push({ edit: true });
    setNewDirs(_newDirs);
  }, [newDirs, setNewDirs]);

  return (
    <>
      {newDirs.map((newDir, idx) => {
        return (
          <SingleNewDir
            key={`${newDir.name}-${idx}`}
            idx={idx}
            name={newDir.name}
            setNewDir={setSingleNewDir}
            edit={newDir.edit}
          />
        );
      })}
      {newDirs.filter((d) => d.edit).length > 0 || (
        <button
          className="ms-auto btn btn-outline-secondary"
          onClick={() => addDir()}
        >
          <i className="bi bi-folder-plus" />
        </button>
      )}
    </>
  );
};

/**
 * Represents the complete directory path of the new file entities
 * (direcotyBase + new directories).
 */
const DirectoryField = ({
  directoryBase,
  readOnly,
  value,
  setDirectory,
  label,
}) => {
  const _label = label || "Directory";
  return (
    <div className="mb-1 row caosdb-f-file-upload-directory-input">
      <label className="col-sm-3 col-form-label">{_label}</label>
      <div className="col-sm-9 d-flex">
        {!directoryBase || (
          <span
            style={{ width: "unset", display: "inline-block" }}
            className="form-control-plaintext"
          >
            {directoryBase.join("/") + "/"}
          </span>
        )}
        {readOnly || <NewDirs setDirectory={setDirectory} value={value} />}
      </div>
    </div>
  );
};

/**
 * Select a RecordType from a drow down.
 */
const Select = ({ options, onChange, value, className }) => {
  return (
    <select className={className} onChange={onChange} value={value}>
      <option value=""></option>
      {options.map((o, idx) => (
        <option key={idx} value={o.value} title={o.title}>
          {o.label || o.value}
        </option>
      ))}
    </select>
  );
};

/**
 * Wraps the Selector and handles asynchronous resolution of the eligible
 * record types.
 */
const RecordTypeField = ({ options, value, onChange, label }) => {
  const _label = label || "RecordType";
  const _readOnly = !options;
  const className = _readOnly
    ? "form-control-plaintext"
    : "form-control form-select";

  return (
    <div className="mb-1 row caosdb-f-file-upload-recordtype-select">
      <label className="pe-1 col-sm-3 col-form-label">{_label}</label>
      <div className="col-sm-9">
        {!_readOnly || (
          <input className={className} readOnly={_readOnly} value={value} />
        )}
        {_readOnly || (
          <Await
            promise={Promise.all([options, value])}
            then={(resolved) => (
              <Select
                className={className}
                options={resolved[0]}
                onChange={onChange}
                value={resolved[1]}
              />
            )}
          />
        )}
      </div>
    </div>
  );
};

/**
 * Single file in the Dropzone.
 */
const File = ({ file, removeFile }) => {
  return <FileMosaic {...file} onDelete={removeFile} />;
};

/**
 * Handle failed file upload.
 */
const FileUploadFailed = ({ children }) => {
  return (
    <Message type="error">
      {"The file upload failed: "}
      {children}
    </Message>
  );
};

/**
 * Handle successful file upload.
 */
const FileUploadSuccess = ({ children }) => {
  return (
    <div>
      {children || (
        <Message type="success">Your files have been uploaded.</Message>
      )}
    </div>
  );
};

/**
 * Show the {name, id, link} of successfully inserted files or {name, error} of
 * failed ones.
 */
const SingleFileResponse = ({ directory, file, responseErrors }) => {
  var result;
  if (responseErrors) {
    result = (
      <>
        <div className="col-sm-6">{file.path.replaceAll(directory, "")}</div>
        <div className="col-sm-6 text-end">
          {file.error ? (
            <div className="text-danger">{file.error}</div>
          ) : (
            <i className="text-success bi bi-check" />
          )}
        </div>
      </>
    );
  } else {
    result = (
      <>
        <div className="col-sm-6">{file.path.replaceAll(directory, "")}</div>
        <div className="col-sm-4">{file.id}</div>
        <div className="col-sm-2 text-end">
          <a className="btn caosdb-fs-btn-file" href={file.url}>
            <span className="badge caosdb-label-file">F</span>
          </a>
        </div>
      </>
    );
  }

  return (
    <li className="list-group-item">
      <div className="row">{result}</div>
    </li>
  );
};

/**
 * Handle the response of the file upload (success and failure).
 */
const handleResponse = (response) => {
  var header = response.hasErrors ? (
    <Message type="error">The file upload failed.</Message>
  ) : (
    <>
      <Message type="success">Your files have been uploaded.</Message>
      {!response.url || (
        <div className="mb-3">
          Go to directory: <a href={response.url}>{response.directory}</a>
        </div>
      )}
    </>
  );
  return (
    <div>
      {header}
      <ul className="list-group">
        <li className="list-group-item">
          <div className="row">
            <div className="col-sm-6"></div>
            {response.hasErrors ? (
              <div className="col-sm-6 text-end fw-bold">Error</div>
            ) : (
              <>
                <div className="col-sm-3 fw-bold">ID</div>
                <div className="col-sm-3 text-end fw-bold">File Entity</div>
              </>
            )}
          </div>
        </li>
        {response.newFiles?.map((f, idx) => (
          <SingleFileResponse
            responseErrors={response.hasErrors}
            directory={response.directory}
            file={f}
            key={idx}
          />
        ))}
      </ul>
    </div>
  );
};

/**
 * Assure that a directory is an array ["dir","subdir"], not a string
 * "dir/subdir".
 */
const toDirectoryArray = (dir) => {
  if (dir?.split) {
    return dir.split("/").filter((x) => x);
  }
  return dir;
};

/**
 * The FileUpload widget.
 */
const FileUpload = ({
  files,
  onCancel,
  recordTypeShow,
  recordTypes,
  directoryBase,
  directory,
  directoryReadOnly,
  directoryShow,
  recordType,
  onChange,
  onSubmit,
  accept,
  state,
  onFinish,
}) => {
  const [_files, setFiles] = useState(files || []);
  const [_directory, setDirectory] = useState(
    toDirectoryArray(directory) || [],
  );
  const [_recordType, setRecordType] = useState(recordType);
  const [_state, setState] = useState(state || "READY");
  const [_error, setError] = useState();
  const [_results, setResults] = useState();

  // convenience object
  const completeState = useMemo(() => {
    return {
      files: _files,
      directory: toDirectoryArray(directoryBase).concat(_directory),
      recordType: _recordType,
    };
  }, [_files, _recordType, _directory, directoryBase]);

  // propagate changes to parent component if necessary
  useEffect(() => {
    if (onChange) {
      onChange(completeState);
    }
  }, [completeState, onChange]);

  const actionButtons = useMemo(() => {
    return {
      position: "after",
      deleteButton: onCancel && {
        className: "btn btn-outline-secondary",
        children: ["Cancel"],
        resetStyles: true,
        onClick: onCancel,
      },
      uploadButton: onSubmit && {
        disabled: !completeState.files?.length > 0,
        className:
          completeState.files?.length > 0
            ? "btn btn-primary"
            : "btn btn-primary disabled",
        resetStyles: true,
        children: ["Upload"],
        onClick: async () => {
          setState("SENDING");
          try {
            const response = await onSubmit({
              ...completeState,
              recordType: await completeState.recordType,
            });
            const results = handleResponse(response);
            setState("SUCCESS");
            setResults(results);
          } catch (error) {
            setState("FAILED");
            setError(<FileUploadFailed>{error.message}</FileUploadFailed>);
          }
        },
      },
    };
  }, [completeState, onSubmit, setState, setError, onCancel]);

  const updateFiles = useCallback(
    (incommingFiles) => {
      setFiles(incommingFiles);
    },
    [setFiles],
  );
  const updateRecordType = useCallback(
    (e) => {
      setRecordType(e.target.value);
    },
    [setRecordType],
  );

  const removeFile = (id) => {
    setFiles(_files.filter((x) => x.id !== id));
  };
  return (
    <div style={{ minWidth: "500px" }}>
      {_state === "SENDING" ? (
        <div className="text-center">
          <LoadingInfo color="primary" />
        </div>
      ) : (
        false
      )}
      {_state === "READY" ? (
        <>
          {(recordTypes || _recordType) && recordTypeShow && (
            <RecordTypeField
              options={recordTypes}
              value={_recordType}
              onChange={updateRecordType}
            />
          )}
          {!directoryShow || (
            <DirectoryField
              directoryBase={toDirectoryArray(directoryBase)}
              readOnly={directoryReadOnly}
              value={_directory}
              setDirectory={setDirectory}
            />
          )}
          <Dropzone
            accept={accept}
            actionButtons={actionButtons}
            onChange={updateFiles}
            value={_files}
          >
            {_files.map((file) => (
              <File key={file.id} file={file} removeFile={removeFile} />
            ))}
          </Dropzone>
        </>
      ) : (
        false
      )}
      {_error}
      {!(_state === "SUCCESS") || (
        <FileUploadSuccess>{_results}</FileUploadSuccess>
      )}
      {(_state === "SUCCESS" || _state === "FAILED") && onFinish && (
        <div className="mt-3 text-end">
          <button
            type="reset"
            title="Close this file upload dialogue."
            className="btn btn-outline-secondary"
            onClick={onFinish}
          >
            Ok
          </button>
        </div>
      )}
    </div>
  );
};

FileUpload.defaultProps = {
  directoryShow: true,
  directoryReadOnly: false,
  recordTypeShow: true,
  directoryBase: [],
  directory: [],
};

export { FileUpload };
