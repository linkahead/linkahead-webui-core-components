# CaosDB WebUI 2 - Core Components

This module contains the core components of the CaosDB WebUI 2.

See [docs.indiscale.com](https://docs.indiscale.com) for more information about the CaosDB Project.

## Development Setup

### Install all necessary stuff

* run `npm install`

### Start the development server

* run `npm run mock`
* go to `http://localhost:8083/` in your browser (or whatever it says on the
  output of the previous command).

### Source Code

All the interesting files are below `src/`

See especially

* [src/index.scss](src/index.scss) - the css file (or rather scss).
* [src/components/LoginForm.jsx](src/components/LoginForm.jsx) - the jsx file of the LoginComponent.
* [src/components/UserComponent.jsx](src/components/UserComponent.jsx) - the jsx file of the UserComponent.

### I18n (Internationalisation)

* We use the [i18next-react](https://react.i18next.com/) framework.
* The configuration is rather static right now and resides in `src/i18n.js`.
* Currently, languages "de" and "en" are supported.
* run `npm run i18n` to update extract keys and update the translation files
  under `./locales/`.

## Copyright

Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>

(unless stated otherwise in the files)

## Licence

AGPL-3.0-or-later
