"use strict";

import "../src/i18n";
import "bootstrap/scss/bootstrap.scss";
import "bootstrap-icons/font/bootstrap-icons.css";
import { mockComponent } from "./helpers.js";

import { LoadingInfo } from "../src/components/LoadingInfo";
import { UserComponent } from "../src/components/UserComponent";
import { Message } from "../src/components/Message";
import { TopNavigationBar } from "../src/components/TopNavigationBar";
import { Checkbox } from "../src/components/Checkbox";
import { QueryForm } from "../src/components/QueryForm";
import { QueryPanel } from "../src/components/QueryPanel";
import { FileUpload } from "../src/components/FileUpload";
import "../src/index.scss";

mockComponent("TopNavigationBar", TopNavigationBar);
mockComponent(
  "UserComponent, unauthenticated. Enter 'error' as username to see an error",
  UserComponent,
);
mockComponent("LoadingInfo, normal size (size='lg')", LoadingInfo);
mockComponent("LoadingInfo, size='sm')", LoadingInfo, { size: "sm" });
mockComponent("Message, default, dismissable", Message, {
  children: "test",
  dismissible: true,
});
mockComponent("Message, error", Message, { type: "error", children: "test" });

mockComponent("Message, success", Message, {
  type: "success",
  children: "test",
});
mockComponent("Message, warning", Message, {
  type: "warning",
  children: "test",
});
mockComponent("Message, with heading, dismissable", Message, {
  heading: "Header",
  children: "test",
  dismissible: true,
});
mockComponent("Message, no icon", Message, { icon: false, children: "test" });
mockComponent("Message, error, no icon", Message, {
  type: "error",
  icon: false,
  children: "test",
});
mockComponent("Message, long body", Message, {
  type: "error",
  dismissible: true,
  heading: "Lorem Ipsum!",
  children:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
});
mockComponent("Checkbox", Checkbox);
mockComponent("Checkbox, value: true, style: x", Checkbox, {
  value: "true",
  style: "x",
});
mockComponent("Checkbox, value: partial, style: x", Checkbox, {
  value: "partial",
  style: "x",
});
mockComponent("Checkbox, value: true, style: x-fill", Checkbox, {
  value: "true",
  style: "x-fill",
});
mockComponent("Checkbox, value: partial, style: x-fill", Checkbox, {
  value: "partial",
  style: "x-fill",
});

mockComponent("Checkbox, value: true, style: check", Checkbox, {
  value: "true",
  style: "check",
});
mockComponent("Checkbox, value: partial, style: check", Checkbox, {
  value: "partial",
  style: "check",
});
mockComponent("Checkbox, value: true, style: check-fill", Checkbox, {
  value: "true",
  style: "check-fill",
});
mockComponent("Checkbox, value: partial, style: check-fill", Checkbox, {
  value: "partial",
  style: "check-fill",
});
mockComponent("QueryForm", QueryForm);
mockComponent("QueryPanel", QueryPanel);
mockComponent("FileUpload", FileUpload, {
  /*files:
    [
      { id: ":0:",
        size: 28 * 1024 * 1024,
        type: "text/plain",
        name: "mock.jsx"
      }
    ],*/
  directoryBase: "somedir/somedir2",
  //directoryShow: false,
  directory: "somedir3",
  //directoryReadOnly: true,
  recordTypes: (async () => [
    { value: "RT1" },
    { value: "RT2" },
    { value: "RT3" },
  ])(),
  //recordType: "RT2",
  onCancel: (e) => {
    console.log(e);
  },
  onSubmit: (e) => {
    console.log(e);
    return {
      newFiles: [{ id: "0", path: "/blub/file.pdf", url: "#file.pdf" }],
      hasErrors: true,
      url: "#blub.dir",
      directory: "/blub/",
    };
  },
  accept: "application/pdf",
  onFinish: console.log,
});
