/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2022-2023 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022-2023 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable react/prop-types */
import React, {
  useMemo,
  useCallback,
  useReducer,
  useRef,
  useState,
  useEffect,
  useContext,
} from "react";
import { LoadingInfo } from "./LoadingInfo";
import getCaretCoordinates from "textarea-caret";
import { AutoCompletion } from "./QueryForm.helpers";
import {
  allowed_operators,
  isSelect as fIsSelect,
  isCql as fIsCql,
  get_test_suggestions,
  get_test_tabs,
} from "./QueryForm.helpers";

const QueryFormContext = React.createContext({});

const RangeFilterInput = (props) => {
  const { onChange, value } = props;
  const onChangeMin = useCallback(
    (e) => {
      const newE = { target: { value: [e.target.value, value[1]] } };
      onChange(newE);
    },
    [onChange, value],
  );
  const onChangeMax = useCallback(
    (e) => {
      const newE = { target: { value: [value[0], e.target.value] } };
      onChange(newE);
    },
    [onChange, value],
  );

  return (
    <>
      <input
        type="number"
        {...props}
        value={props.value[0]}
        onChange={onChangeMin}
        max={props.value[1]}
      />
      <span className="mx-1">to</span>
      <input
        type="number"
        {...props}
        value={props.value[1]}
        onChange={onChangeMax}
        min={props.value[0]}
      />
      {props.unitElement}
    </>
  );
};

const FilterInput = ({
  min,
  max,
  step,
  value,
  setValue,
  datatype,
  operator,
  unit,
  inputtype,
}) => {
  const onChange = useCallback((e) => setValue(e.target.value), [setValue]);
  if (!datatype || !operator) {
    return undefined;
  }

  const defaultProps = {
    className: "form-control form-control-sm",
    onChange: onChange,
    value: value,
    min: min,
    max: max,
  };
  if (inputtype) {
    defaultProps.type = inputtype;
  }
  if (step) {
    defaultProps.step = step;
  }

  const unitElement = unit && (
    <span className="ms-1" title="The unit">
      {unit}
    </span>
  );

  const inputElement = {
    boolean: { _default: null },
    integer: {
      _default: (
        <>
          <input {...defaultProps} type="number" step="1" />
          {unitElement}
        </>
      ),
      range: (
        <RangeFilterInput
          unitElement={unitElement}
          {...defaultProps}
          step="1"
        />
      ),
    },
    double: {
      _default: (
        <>
          <input step="0.001" {...defaultProps} type="number" />
          {unitElement}
        </>
      ),
      range: (
        <RangeFilterInput
          unitElement={unitElement}
          step="0.001"
          {...defaultProps}
        />
      ),
    },
    _default: { _default: <input {...defaultProps} /> },
  };

  if (inputElement[datatype]) {
    if (
      inputElement[datatype][operator] ||
      inputElement[datatype][operator] === null
    ) {
      return inputElement[datatype][operator];
    } else if (
      inputElement[datatype]["_default"] ||
      inputElement[datatype]["_default"] === null
    ) {
      return inputElement[datatype]["_default"];
    }
  }

  if (inputElement["_default"][operator]) {
    return inputElement["_default"][operator];
  }

  return inputElement["_default"]["_default"];
};

function CqlHint() {
  return (
    <div
      className="caosdb-f-query-cql-hint"
      title="You are using the expert search mode. Enter a valid CQL Query."
    >
      CQL
    </div>
  );
}

function QueryFormSingleTab({ tab, setActiveTab, isActiveTab }) {
  const activateTab = useCallback(() => {
    if (!isActiveTab) {
      setActiveTab(tab);
    }
  }, [tab, setActiveTab, isActiveTab]);

  const className = "caosdb-f-query-tab" + (isActiveTab ? " active" : "");
  return (
    <button
      type="button"
      title={tab.description}
      tabIndex={isActiveTab ? "-1" : "0"}
      className={className}
      onClick={isActiveTab ? undefined : activateTab}
    >
      {tab.label}
    </button>
  );
}

function AddTabFilterOperator({
  filter,
  filterIdx,
  updateFilter,
  restoreOperator,
  restoreValue,
}) {
  const [operator, setOperator] = useState(
    filter.operator || allowed_operators[filter.datatype][0],
  );
  const [value, setValue] = useState(filter.value || "");

  const inputElement = (
    <FilterInput
      datatype={filter.datatype}
      operator={operator}
      setValue={setValue}
      value={value}
      unit={filter.unit}
      min={filter.min}
      max={filter.max}
      step={filter.step}
      inputtype={filter.inputtype}
    />
  );

  useEffect(() => {
    if (restoreOperator && operator !== restoreOperator) {
      setOperator(restoreOperator);
    }
  }, [restoreOperator, operator]);

  useEffect(() => {
    if (restoreValue && value !== restoreValue) {
      setValue(restoreValue);
    }
  }, [restoreValue, value]);

  useEffect(() => {
    if (operator !== filter.operator) {
      updateFilter(filterIdx, {
        restoreOperator: undefined,
        operator: operator,
      });
    }
  }, [operator, filter.operator, filterIdx, updateFilter]);

  useEffect(() => {
    if (value !== filter.value) {
      updateFilter(filterIdx, {
        restoreValue: undefined,
        value: value,
      });
    }
  }, [value, filter.value, filterIdx, updateFilter]);

  return (
    <>
      <div>
        <select
          className="form-select form-select-sm"
          value={operator}
          onChange={(e) => setOperator(e.target.value)}
        >
          {allowed_operators[filter.datatype].map((op, idx) => (
            <option key={idx}>{op}</option>
          ))}
        </select>
      </div>
      <div className="d-flex">{inputElement}</div>
    </>
  );
}

function FilterSelect({ selected, filterDefinitions, onChange, title }) {
  return (
    <select
      title={title}
      className="form-select form-select-sm"
      value={selected}
      onChange={onChange}
    >
      {filterDefinitions.map((fil, idx) => (
        <option value={fil.property} style={fil.style} key={idx}>
          {fil.label || fil.property}
        </option>
      ))}
    </select>
  );
}

function AddFilter({ filterDefinitions, addFilter }) {
  const onChange = useCallback(
    (e) => {
      addFilter(e.target.value);
    },
    [addFilter],
  );
  return (
    <div>
      <FilterSelect
        filterDefinitions={filterDefinitions.concat({
          property: "__add_filter__",
          label: "Add Filter",
          style: { display: "none" },
        })}
        selected="__add_filter__"
        title="Add Filter"
        onChange={onChange}
      />
    </div>
  );
}

function Filter({
  filterDefinitions,
  filterIdx,
  activeFilters,
  updateFilter,
  setProperty,
  removeFilter,
}) {
  const activeFilter = activeFilters && activeFilters[filterIdx];
  const onChange = useCallback(
    (e) => {
      setProperty(filterIdx, e.target.value);
    },
    [setProperty, filterIdx],
  );
  const removeThis = useCallback(
    () => removeFilter(filterIdx),
    [removeFilter, filterIdx],
  );

  return (
    <>
      <div className="caosdb-f-query-filter-property-selector">
        <FilterSelect
          filterDefinitions={filterDefinitions}
          selected={activeFilter.property}
          onChange={onChange}
        />
      </div>
      <AddTabFilterOperator
        restoreValue={activeFilter.restoreValue}
        restoreOperator={activeFilter.restoreOperator}
        updateFilter={updateFilter}
        filterIdx={filterIdx}
        filter={activeFilter}
      />
      <div className="text-end flex-grow-1">
        <button
          type="button"
          className="btn btn-sm btn-outline-secondary"
          onClick={removeThis}
        >
          Remove Filter
        </button>
      </div>
    </>
  );
}

function QueryFormTabFiltersInner({
  filterDefinitions,
  activeFilters,
  setActiveFilters,
}) {
  const removeFilter = useCallback(
    (filterIdx) => {
      setActiveFilters(activeFilters.filter((fil, idx) => idx !== filterIdx));
    },
    [activeFilters, setActiveFilters],
  );

  const updateFilter = useCallback(
    (filterIdx, update) => {
      const newActiveFilters = [].concat(activeFilters);

      newActiveFilters[filterIdx] = {
        ...newActiveFilters[filterIdx],
        ...update,
      };

      setActiveFilters(newActiveFilters);
    },
    [activeFilters, setActiveFilters],
  );

  const setProperty = useCallback(
    (filterIdx, propertyName) => {
      const propertyFilter = filterDefinitions.filter(
        (fil) => fil.property === propertyName,
      )[0];
      updateFilter(filterIdx, {
        restoreValue: "",
        restoreOperator: allowed_operators[propertyFilter.datatype][0],
        property: propertyName,
        datatype: propertyFilter.datatype,
        unit: propertyFilter.unit,
        min: propertyFilter.min,
        max: propertyFilter.max,
        filter: propertyFilter,
      });
    },
    [updateFilter, filterDefinitions],
  );

  const addFilter = useCallback(
    (propertyName) => {
      const propertyFilter = filterDefinitions.filter(
        (fil) => fil.property === propertyName,
      )[0];
      setActiveFilters(
        activeFilters.concat({
          property: propertyName,
          datatype: propertyFilter.datatype,
          unit: propertyFilter.unit,
          min: propertyFilter.min,
          max: propertyFilter.max,
          filter: propertyFilter,
        }),
      );
    },
    [activeFilters, setActiveFilters, filterDefinitions],
  );

  return (
    <>
      {activeFilters.map((fil, idx) => (
        <Filter
          key={idx}
          filterIdx={idx}
          filterDefinitions={filterDefinitions}
          activeFilters={activeFilters}
          setActiveFilters={setActiveFilters}
          updateFilter={updateFilter}
          setProperty={setProperty}
          removeFilter={removeFilter}
        />
      ))}
      <div className="d-flex">
        <AddFilter
          filterDefinitions={filterDefinitions}
          addFilter={addFilter}
        />
        {activeFilters.length > 0 && (
          <button
            type="submit"
            title="Search with filters"
            className="btn btn-primary btn-sm ms-auto"
          >
            Search
          </button>
        )}
      </div>
    </>
  );
}

function QueryFormTabFilters({
  filterDefinitions,
  activeFilters,
  setActiveFilters,
}) {
  const [showFilters, setShowFilters] = useState(activeFilters.length > 0);

  const title = showFilters
    ? "Cancel Advanced Filtering"
    : "Advanced Filtering";

  return (
    <>
      <button
        type="button"
        onClick={() => setShowFilters(!showFilters)}
        className="btn ms-auto caosdb-f-query-filters-toggle"
        title={title}
      >
        {showFilters ? (
          <i className="bi bi-x"></i>
        ) : (
          <i className="bi bi-filter"></i>
        )}
      </button>
      {showFilters && (
        <div className="caosdb-f-query-tab-filters w-100">
          <QueryFormTabFiltersInner
            filterDefinitions={filterDefinitions}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
          />
        </div>
      )}
    </>
  );
}

function QueryFormTabs({
  tabs,
  setActiveTab,
  activeTab,
  activeFilters,
  setActiveFilters,
}) {
  const hasFilters = useMemo(() => {
    return (
      activeTab &&
      activeTab.filterDefinitions &&
      activeTab.filterDefinitions.length > 0
    );
  }, [activeTab]);
  return (
    <div className="caosdb-f-query-tab-list d-flex flex-wrap">
      {tabs.map((tab) => (
        <QueryFormSingleTab
          isActiveTab={activeTab.id === tab.id}
          setActiveTab={setActiveTab}
          key={tab.label}
          tab={tab}
        />
      ))}{" "}
      {hasFilters && (
        <QueryFormTabFilters
          {...activeTab}
          activeFilters={activeFilters}
          setActiveFilters={setActiveFilters}
        />
      )}
    </div>
  );
}

function QuerySubmitButton({ isSubmitted, active }) {
  if (isSubmitted) {
    return <LoadingInfo className="align-self-center" size="sm" />;
  }
  return (
    <button aria-label="search" title="Search" type="submit" disabled={!active}>
      <i className="bi bi-search"></i>
    </button>
  );
}

function SingleQueryFormSuggestionsItem({
  suggestion,
  isHighlighted,
  onMouseEnter,
}) {
  return (
    <li
      onMouseEnter={onMouseEnter}
      className={isHighlighted ? "highlight" : ""}
    >
      {suggestion.original}
    </li>
  );
}

function QueryFormSuggestionsItems({
  suggestions,
  highlightIndex,
  selectSuggestion,
}) {
  return (
    <>
      {suggestions.map((sug, index) => (
        <SingleQueryFormSuggestionsItem
          onMouseEnter={() => selectSuggestion(index)}
          key={index}
          isHighlighted={highlightIndex === index}
          suggestion={sug}
        />
      ))}
    </>
  );
}

function QueryFormSuggestions({
  unselectSuggestions,
  selectSuggestion,
  highlightIndex,
  suggestions,
  getInputRef,
  wordPos,
  word,
  xOffset,
}) {
  const ref = useRef();
  const items = (
    <QueryFormSuggestionsItems
      selectSuggestion={selectSuggestion}
      highlightIndex={highlightIndex}
      suggestions={suggestions}
    />
  );

  useEffect(() => {
    if (highlightIndex > -1) {
      ref.current.scroll(0, 24 * highlightIndex - 48);
    }
  }, [highlightIndex]);
  useEffect(() => {
    const inputRef = getInputRef();
    var coords = getCaretCoordinates(inputRef.current, wordPos);
    const inputWidth = inputRef.current.getBoundingClientRect().width;
    if (coords.left > inputWidth) {
      const rightOffset =
        getCaretCoordinates(inputRef.current, wordPos + word.length).left -
        coords.left;

      ref.current.style.marginLeft = `${inputWidth - rightOffset}px`;
    } else if (coords.left > 8) {
      ref.current.style.marginLeft = `${coords.left - 8}px`;
    }
  }, [word, getInputRef, wordPos]);

  return (
    <div
      onMouseLeave={unselectSuggestions}
      style={{ paddingLeft: xOffset }}
      className="caosdb-f-query-form-sug-container"
    >
      <ol ref={ref} className="caosdb-f-query-form-sug">
        {items}
      </ol>
    </div>
  );
}

export function reducer(state, action) {
  var cursor_pos, word_pos, word, open, unresolved_suggestions;
  switch (action.type) {
    case "onFocus":
      return { ...state, refocus: false, focus: true };
    case "onBlur":
      if (state.onClickValue) {
        return {
          ...state,
          value: state.onClickValue,
          cursorPos: state.onClickCursorPos,
          onClickCursorPos: undefined,
          onClickValue: undefined,
          refocus: true,
          open: false,
        };
      }
      return { ...state, focus: false };
    case "onChange":
      cursor_pos = action.target.selectionEnd;
      word_pos = action.target.value.slice(0, cursor_pos).lastIndexOf(" ") + 1;
      word = action.target.value.slice(word_pos, cursor_pos);
      open = word.length > 2;
      unresolved_suggestions =
        open && action.getSuggestionsCallback
          ? action.getSuggestionsCallback(
              fIsCql(action.target.value),
              action.target.value,
              word,
            )
          : null;

      return {
        ...state,
        onClickValue: undefined,
        onClickCursorPos: undefined,
        highlightIndex: -1,
        cursorPos: cursor_pos,
        cursorPosNoCompletion: cursor_pos,
        wordPos: word_pos,
        value: action.target.value,
        valueNoCompletion: action.target.value,
        refocus: false,
        word: word,
        open: open,
        changeId: (state.changeId + 1) | 0,
        unresolvedSuggestions: unresolved_suggestions,
      };
    case "resolveSuggestions":
      if (state.changeId === action.changeId) {
        return {
          ...state,
          suggestions: action.suggestions,
          unresolvedSuggestions: undefined,
        };
      }
      // else { /* suggestions are outdated due to new changes */ }
      break;
    case "onArrowDown":
      if (
        state.open &&
        state.suggestions &&
        state.highlightIndex < state.suggestions.length - 1
      ) {
        const newHighlightIndex = state.highlightIndex + 1;
        const newValue =
          state.valueNoCompletion.slice(0, state.wordPos) +
          state.suggestions[newHighlightIndex].escaped +
          state.valueNoCompletion.slice(state.cursorPosNoCompletion);
        const newCursorPos =
          state.wordPos + state.suggestions[newHighlightIndex].escaped.length;
        return {
          ...state,
          cursorPos: newCursorPos,
          value: newValue,
          highlightIndex: newHighlightIndex,
        };
      }
      break;
    case "unselectSuggestions":
      return {
        ...state,
        onClickCursorPos: undefined,
        onClickValue: undefined,
      };
    case "selectSuggestion":
      if (action.index > -1 && action.index < state.suggestions.length) {
        const newValue =
          state.valueNoCompletion.slice(0, state.wordPos) +
          state.suggestions[action.index].escaped +
          state.valueNoCompletion.slice(state.cursorPosNoCompletion) +
          " ";
        const newCursorPos =
          state.wordPos + state.suggestions[action.index].escaped.length + 1;
        return {
          ...state,
          onClickValue: newValue,
          onClickCursorPos: newCursorPos,
        };
      }
      break;
    case "restore":
      return { ...state, value: action.value };
    case "onArrowUp":
      if (state.open && state.highlightIndex > -1) {
        const newHighlightIndex = state.highlightIndex - 1;
        const newValue =
          newHighlightIndex < 0
            ? state.valueNoCompletion
            : state.valueNoCompletion.slice(0, state.wordPos) +
              state.suggestions[newHighlightIndex].escaped +
              state.valueNoCompletion.slice(state.cursorPosNoCompletion);
        const newCursorPos =
          newHighlightIndex < 0
            ? state.cursorPosNoCompletion
            : state.wordPos +
              state.suggestions[newHighlightIndex].escaped.length;
        return {
          ...state,
          cursorPos: newCursorPos,
          value: newValue,
          highlightIndex: newHighlightIndex,
        };
      }
      break;
    default:
      break;
  }
  return state;
}

function QueryFormTextField({ setQueryString, isCql, restoreQueryString }) {
  const { getSuggestionsCallback, isSubmitted } = useContext(QueryFormContext);
  const inputRef = useRef();
  const containerRef = useRef();
  const [state, dispatch] = useReducer(reducer, {
    refocus: false,
    value: "",
    focus: false,
    open: false,
    word: "",
    highlightIndex: -1,
    unresolvedSuggestions: null,
    suggestions: null,
  });

  useEffect(() => {
    setQueryString(state.value);
  }, [state.value, setQueryString]);

  useEffect(() => {
    if (restoreQueryString) {
      dispatch({ type: "restore", value: restoreQueryString });
    }
  }, [restoreQueryString]);

  const onFocus = useCallback(() => {
    dispatch({ type: "onFocus" });
  }, []);

  const onBlur = useCallback(() => {
    dispatch({ type: "onBlur" });
  }, []);

  const onArrowUp = useCallback(() => {
    dispatch({ type: "onArrowUp" });
  }, []);

  const onArrowDown = useCallback(() => {
    dispatch({ type: "onArrowDown" });
  }, []);

  const onChange = useCallback(
    (e) => {
      dispatch({
        type: "onChange",
        target: e.target,
        getSuggestionsCallback: getSuggestionsCallback,
      });
    },
    [getSuggestionsCallback],
  );

  const onKeyDown = useCallback(
    (e) => {
      if (e.key === "ArrowDown") {
        e.preventDefault();
        return onArrowDown(e);
      } else if (e.key === "ArrowUp") {
        e.preventDefault();
        return onArrowUp(e);
      }
    },
    [onArrowUp, onArrowDown],
  );

  const unselectSuggestions = useCallback(() => {
    dispatch({ type: "unselectSuggestions" });
  }, []);

  const selectSuggestion = useCallback((index) => {
    dispatch({ type: "selectSuggestion", index: index });
  }, []);

  var className = "caosdb-f-query-form";
  if (state.open) {
    className += " open";
  }
  if (state.focus) {
    className += " focus";
  }

  useEffect(() => {
    if (state.unresolvedSuggestions) {
      const changeId = state.changeId;
      state.unresolvedSuggestions.then((suggestions) => {
        dispatch({
          type: "resolveSuggestions",
          suggestions: suggestions,
          changeId: changeId,
        });
      });
    }
  }, [state.unresolvedSuggestions, state.changeId]);

  useEffect(() => {
    inputRef.current.setSelectionRange(state.cursorPos, state.cursorPos);
  }, [state.value, state.cursorPos]);

  const getInputRef = useCallback(() => {
    return inputRef;
  }, [inputRef]);

  // handle focus after reset
  useEffect(() => {
    if (state.refocus) {
      inputRef.current.focus();
    }
  }, [state]);

  return (
    <div ref={containerRef} className={className}>
      <div className="d-flex">
        {isCql && <CqlHint />}
        <input
          disabled={isSubmitted}
          className="flex-grow-1"
          aria-label="enter search terms"
          name="query"
          title="Enter search terms"
          onFocus={onFocus}
          onBlur={onBlur}
          onKeyDown={onKeyDown}
          onChange={onChange}
          value={state.value}
          ref={inputRef}
        ></input>
        <QuerySubmitButton
          active={state.value.length > 0}
          isSubmitted={isSubmitted}
        />
      </div>
      {state.open && state.suggestions && (
        <QueryFormSuggestions
          unselectSuggestions={unselectSuggestions}
          selectSuggestion={selectSuggestion}
          word={state.word}
          wordPos={state.wordPos}
          getInputRef={getInputRef}
          suggestions={state.suggestions}
          highlightIndex={state.highlightIndex}
          xOffset={isCql ? 44 : 0}
        />
      )}
    </div>
  );
}

function store(activeTab, activeFilters, queryString) {
  window.localStorage.removeItem("query.activeTab");
  window.localStorage.removeItem("query.activeFilters");
  window.localStorage.removeItem("query.queryString");
  if (activeTab) {
    window.localStorage["query.activeTab"] = JSON.stringify(activeTab);
  }
  if (activeFilters && activeFilters.length > 0) {
    window.localStorage["query.activeFilters"] = JSON.stringify(activeFilters);
  }
  if (queryString && queryString.length > 0) {
    window.localStorage["query.queryString"] = queryString;
  }
}

function restore(tabs) {
  const result = {
    restoreActiveTab:
      window.localStorage["query.activeTab"] &&
      JSON.parse(window.localStorage["query.activeTab"]),
    restoreActiveFilters:
      window.localStorage["query.activeFilters"] &&
      JSON.parse(window.localStorage["query.activeFilters"]),
    restoreQueryString: window.localStorage["query.queryString"] || "",
  };
  if (
    result.restoreActiveTab &&
    tabs.filter((tab) => tab.id === result.restoreActiveTab.id).length === 0
  ) {
    // config changed
    window.localStorage.removeItem("query.activeTab");
    window.localStorage.removeItem("query.activeFilters");
    window.localStorage.removeItem("query.queryString");
    return {};
  }
  return result;
}

export function QueryForm({
  submitCallback,
  tabs,
  defaultTab,
  getSuggestionsCallback,
  restoreCallback,
  storeCallback,
  restore,
}) {
  const { restoreQueryString, restoreActiveFilters, restoreActiveTab } = restore
    ? restoreCallback(tabs)
    : {};
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [queryString, setQueryString] = useState("");
  const [activeTab, setActiveTab] = useState(
    restoreActiveTab || tabs[defaultTab],
  );
  const [activeFilters, setActiveFilters] = useState(
    restoreActiveFilters || [],
  );

  const onSubmit = useCallback(
    (e) => {
      e.preventDefault();
      setIsSubmitted(true);
      storeCallback(activeTab, activeFilters, queryString);
      const activeTabDef = tabs.filter((t) => t.id === activeTab.id)[0] || {};
      const isCql = fIsCql(queryString);
      const isSelect = isCql && fIsSelect(queryString);
      const makeQuery =
        isCql || !activeTabDef.makeQuery ? (x) => x : activeTabDef.makeQuery;
      const pageSize = isSelect ? -1 : undefined;

      return submitCallback(makeQuery(queryString, activeFilters), pageSize);
    },
    [
      tabs,
      activeTab,
      activeFilters,
      queryString,
      submitCallback,
      storeCallback,
    ],
  );

  const isCql = fIsCql(queryString);

  const context = useMemo(() => {
    return {
      isSubmitted: isSubmitted,
      queryString: queryString,
      getSuggestionsCallback: getSuggestionsCallback,
    };
  }, [queryString, getSuggestionsCallback, isSubmitted]);

  const setActiveTabResetFilters = useCallback(
    (tab) => {
      setActiveTab(tab);
      setActiveFilters([]);
    },
    [setActiveFilters, setActiveTab],
  );

  return (
    <form autoComplete="off" onSubmit={onSubmit}>
      <QueryFormContext.Provider value={context}>
        <QueryFormTextField
          restoreQueryString={restoreQueryString}
          isCql={isCql}
          setQueryString={setQueryString}
        />
        {!isCql && (
          <QueryFormTabs
            tabs={tabs}
            activeTab={activeTab}
            setActiveTab={setActiveTabResetFilters}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
          />
        )}
      </QueryFormContext.Provider>
    </form>
  );
}

QueryForm.AutoCompletion = AutoCompletion;

// Defaults are suitable for testing but not for production.
QueryForm.defaultProps = {
  submitCallback: (queryString, pageSize) => {
    console.log("submit query: ", queryString, pageSize);
    setTimeout(() => {
      alert(queryString);
      // reload page
      // eslint-disable-next-line no-self-assign
      window.location.href = window.location.href;
    }, 1000);
  },
  tabs: get_test_tabs(),
  defaultTab: 0,
  getSuggestionsCallback: get_test_suggestions,
  restoreCallback: restore,
  storeCallback: store,
  restore: false,
};
