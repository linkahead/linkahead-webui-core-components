import React from "react";
import PropTypes from "prop-types";

export const LoadingInfo = ({ className, size, color, title }) => {
  var classes = "spinner-border";

  if (className) {
    classes += " " + className;
  }
  if (size === "sm") {
    classes += " spinner-border-sm";
  }
  if (color === "primary") {
    classes += " text-primary";
  } else if (color === "secondary") {
    classes += " text-secondary";
  }

  return (
    <span title={title} className={classes} role="status" aria-hidden="true" />
  );
};

LoadingInfo.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(["sm"]),
  color: PropTypes.oneOf(["primary", "secondary"]),
  title: PropTypes.string,
};

LoadingInfo.defaultProps = {
  title: "Loading...",
};
