import { i18n } from "../i18n";
import React, { useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { InfoService } from "../InfoService";
import { LoginForm } from "./LoginForm";
import { LoadingInfo } from "./LoadingInfo";
import { NavDropdown } from "react-bootstrap";

/**
 * Work-around for translation of common server messages until the server can
 * do it.
 */
function translateServerMessages(message) {
  switch (message) {
    case "Authentication failed. Username or password wrong.":
      return i18n.t("Authentication failed. Username or password wrong"); // no dot at the end
    case "Authentication failed. SessionToken was invalid.":
      return i18n.t("Authentication failed. SessionToken was invalid"); // no dot at the end
    case "Please log in!":
      return i18n.t("Please log in!");
    default:
      return message;
  }
}

function _UserComponent({ session_info, logout_callback }) {
  const { t } = useTranslation();

  const title = (
    <>
      {session_info.username} <i className="bi bi-person-fill" />
    </>
  );
  return (
    <NavDropdown
      align="end"
      aria-label={t("Open user menu", { session_info: session_info })}
      title={title}
    >
      <NavDropdown.Item
        aria-label={t("Logout")}
        title={t("Click to log out")}
        as="button"
        onClick={logout_callback}
        href="#"
      >
        {t("Logout")}
      </NavDropdown.Item>
    </NavDropdown>
  );
}

_UserComponent.propTypes = {
  session_info: PropTypes.shape({
    username: PropTypes.string,
  }),
  logout_callback: PropTypes.func,
}

/**
 * @function onLogoutCallback
 *
 * To be used as a callback function after a successful logout.
 *
 * @return {boolean}
 */

/**
 * @function onLoginCallback
 *
 * To be used as a callback function after a successful login.
 *
 * @param {@indiscale/caosdb-webui-info-service.SessionInfo} sessionInfo about
 *   the current user.
 * @return {boolean}
 */

/**
 * The UserComponent shows the information about the current user or a
 * LoginForm.
 *
 * @param {onLoginCallback} onLogin Callback function which will be
 *   called after a successful login. When the function returns true, the
 *   UserComponent proceeds as usual and re-renders the component.
 * @param {onLogoutCallback} onLogout Callback function which will be
 *   called after a successful logout. When the function returns true, the
 *   UserComponent proceeds as usual and re-renders the component.
 */
export function UserComponent({ onLogin, onLogout }) {
  const { t } = useTranslation();
  const [component, setComponent] = useState(null);

  var show_component = component;
  if (component === null) {
    var show_login_form;

    const logout_callback = async () => {
      try {
        setComponent(<LoadingInfo size="sm" />);
        const service = new InfoService();
        await service.logout();
        if (onLogout()) {
          show_login_form(t("You have been logged out"), "success");
        }
      } catch (e) {
        show_login_form(
          t("Logout failed") + ": " + translateServerMessages(e.message),
          "error",
        );
      }
    };

    const show_session_info = (session_info) => {
      if (session_info.username === "anonymous") {
        show_login_form();
      } else {
        setComponent(
          <_UserComponent
            session_info={session_info}
            logout_callback={logout_callback}
          />,
        );
      }
    };

    const login_fail = (error) => {
      show_login_form(translateServerMessages(error.message), "error");
    };

    const login_success = (session_info) => {
      if (onLogin(session_info)) {
        show_session_info(session_info);
      }
    };

    show_login_form = (message, messageType) => {
      var login_form = (
        <LoginForm
          message={message}
          messageType={messageType}
          then={login_success}
          onError={login_fail}
        />
      );
      setComponent(login_form);
    };

    const service = new InfoService();
    service
      .getSessionInfo()
      .then(show_session_info)
      .catch((error) =>
        show_login_form(translateServerMessages(error.message), "info"),
      );
    show_component = <LoadingInfo size="sm" />;
  }

  return <div className="user-component">{show_component}</div>;
}

UserComponent.propTypes = {
  onLogin: PropTypes.func,
  onLogout: PropTypes.func,
};

UserComponent.defaultProps = {
  onLogin: () => true,
  onLogout: () => true,
};
