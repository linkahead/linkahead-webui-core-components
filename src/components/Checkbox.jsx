import React, { useState } from "react";
import PropTypes from "prop-types";

export const Checkbox = ({
  name,
  value,
  icons,
  style,
  color,
  readOnly,
  onChange,
  onBlur,
  setValue,
  title,
}) => {
  const [state, setState] = useState(value || "false");
  style = style || "check";
  color =
    color ||
    {
      check: {
        true: " text-success",
        false: " text-body",
        partial: " color-light-green",
      },
      "check-fill": {
        true: " text-success",
        false: " text-body",
        partial: " color-light-green",
      },
      x: {
        true: " text-danger",
        false: " text-body",
        partial: " text-warning",
      },
      "x-fill": {
        true: " text-danger",
        false: " text-body",
        partial: " text-warning",
      },
    }[style];

  const iconClassNameMap = {
    x: {
      true: "bi bi-x-square" + color["true"],
      false: "bi bi-square" + color["false"],
      partial: "bi bi-slash-square" + color["partial"],
    },
    "x-fill": {
      true: "bi bi-x-square-fill" + color["true"],
      false: "bi bi-square" + color["false"],
      partial: "bi bi-slash-square-fill" + color["partial"],
    },
    check: {
      true: "bi bi-check-square" + color["true"],
      false: "bi bi-square" + color["false"],
      partial: "bi bi-slash-square" + color["partial"],
    },
    "check-fill": {
      true: "bi bi-check-square-fill" + color["true"],
      false: "bi bi-square" + color["false"],
      partial: "bi bi-slash-square-fill" + color["partial"],
    },
  };

  const transitions = {
    partial: "false",
    true: "false",
    false: "true",
  };

  const props = {};

  var input;
  if (!readOnly) {
    input = (
      <input
        className="caosdb-check-box-input"
        onChange={onChange}
        tabIndex="-1"
        type="hidden"
        name={name}
        value={state}
      />
    );

    props.onClick = () => {
      setValue(name, transitions[state]);
      setState(transitions[state]);
    };

    props.onKeyDown = (e) => {
      if (e.code === "Space") {
        setValue(name, transitions[state]);
        setState(transitions[state]);
        e.preventDefault();
      }
    };

    props.tabIndex = "0";
    props.onBlur = onBlur;
  }

  icons = icons || {};

  const icon = icons[state] || (
    <i
      className={"ms-1 caosdb-checkbox-icon " + iconClassNameMap[style][state]}
    />
  );

  var className = "caosdb-checkbox d-flex align-items-center";
  if (readOnly) {
    className += " read-only";
  }
  return (
    <span
      title={title}
      className={className}
      role="checkbox"
      aria-checked={state}
    >
      {input}
      <span {...props}>{icon}</span>
    </span>
  );
};

Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOf(["true", "false"]),
  icons: PropTypes.object,
  style: PropTypes.string,
  color: PropTypes.object,
  readOnly: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  setValue: PropTypes.func,
  title: PropTypes.string,
};
