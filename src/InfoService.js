var InfoService;

class MockupInfoServiceImpl {
  async getSessionInfo() {
    await this.sleep(3000);
    throw { message: "Please log in!" };
  }

  sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async login(username) {
    await this.sleep(3000);
    if (username == "error") {
      throw { message: "Login failed! Ahhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh!" };
    }
    return {
      realm: "MockRealm",
      username: username,
      roles: [],
      expires: 60000,
    };
  }
  async logout() {
    await this.sleep(3000);
  }
}

try {
  InfoService = require("@indiscale/caosdb-webui-info-service").InfoService;
} catch (error) {
  console.log("Providing mockup implementation for InfoService");

  InfoService = MockupInfoServiceImpl;
}

export { InfoService };
