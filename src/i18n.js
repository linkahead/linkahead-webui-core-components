import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import resourcesToBackend from "i18next-resources-to-backend";

export const ready = i18n
  .use(initReactI18next)
  .use(
    resourcesToBackend((language, namespace) =>
      import(`../locales/${language}/${namespace}.json`),
    ),
  )
  .init({
    supportedLngs: ["en", "de"],
    lng: "en",

    interpolation: {
      escapeValue: false,
    },
    react: {
      useSuspense: true,
    },
  });

export async function changeLanguage(lng) {
  await ready;
  await i18n.changeLanguage(lng);
}
export { i18n };
export default i18n;
