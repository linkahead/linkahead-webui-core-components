import React from "react";
import { TopNavigationBar } from "./TopNavigationBar";

export const Header = () => {
  return (
    <header className="shadow-sm bg-light">
      <TopNavigationBar></TopNavigationBar>
    </header>
  );
};
