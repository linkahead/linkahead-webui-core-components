import { i18n, ready, changeLanguage } from "../i18n";

test("i18n", async () => {
  await ready;
  expect(i18n.t("Login", { defaultValue: "n/a" })).toBe("Login");
  expect(
    i18n.t("Authentication failed. Username or password wrong", {
      defaultValue: "n/a",
    }),
  ).toBe("Authentication failed. Username or password wrong.");

  await changeLanguage("de");
  expect(i18n.t("Login", { defaultValue: "n/a" })).toBe("Anmelden");
  expect(
    i18n.t("Authentication failed. Username or password wrong", {
      defaultValue: "n/a",
    }),
  ).toBe("Anmeldung fehlgeschlagen. Username oder passwort falsch.");
});
