import React from "react";

class FooterManagerImpl {
  constructor({ items, copyRight }) {
    this.items = items;
    this.copyRight = copyRight;
  }
}

export const FooterManager = new FooterManagerImpl({
  items: [
    <a
      key="0"
      className="nav-link px-2"
      href="https://www.gnu.org/licenses/agpl-3.0.en.html"
    >
      License (AGPL-v3)
    </a>,
    <a key="1" className="nav-link px-2" href="https://gitlab.com/caosdb">
      Sources
    </a>,
    <a key="3" className="nav-link px-2" href="https://docs.indiscale.com/">
      Documentation
    </a>,
  ],
  copyRight: "© 2021 IndiScale GmbH",
});

export const Footer = () => {
  return (
    <footer>
      <div className="footer-border" />
      <div className="footer-body">
        <div className="container py-3 my-4">
          <ul className="nav align-items-center justify-content-center pb-3 mb-3">
            {FooterManager.items.map((item, index) => {
              const spacer =
                index === 0 ? null : <li className="nav-item px-1">•</li>;
              return (
                <React.Fragment key={index}>
                  {spacer}
                  <li className="nav-item">{item}</li>
                </React.Fragment>
              );
            })}
          </ul>
          <p className="text-center">{FooterManager.copyRight}</p>
        </div>
      </div>
    </footer>
  );
};
