import { AutoCompletion, makeQueryTemplate } from "../QueryForm.helpers";

describe("makeQueryTemplate", () => {
  test("parenthesis around filters", () => {
    const makeQuery = makeQueryTemplate("RT");
    const search_terms = undefined;

    var filters = [
      { datatype: "text", property: "prop1", operator: "=", value: "val1" },
      {
        datatype: "double",
        property: "Event.longitude",
        operator: "range",
        value: [-40, -20],
        unit: "°",
      },
      {
        datatype: "double",
        property: "Event.latitude",
        operator: ">",
        value: 40,
        unit: "°",
      },
      { datatype: "integer", property: "prop2", operator: "≥", value: "1234" },
      { datatype: "boolean", property: "prop3", operator: "is true" },
      {
        datatype: "datetime",
        property: '"prop4"',
        operator: "after date",
        value: "2024",
      },
    ];
    var expected =
      'FIND RECORD "RT" WITH (prop1 = "val1") AND (Event.longitude > -40°) AND (Event.longitude < -20°) AND (Event.latitude > 40°) AND (prop2 >= "1234") AND (prop3 = TRUE) AND ("prop4" > "2024")';
    expect(makeQuery(search_terms, filters)).toBe(expected);
  });
});

describe("AutoCompletion", () => {
  test("terms are being fetched/resolved only once", async () => {
    const mockFetchTerms = jest.fn(
      () =>
        new Promise((resolve) => {
          // takes one second to resolve
          setTimeout(() => resolve(["t-term", "tw-term", "two-term"]), 1000);
        }),
    );
    const auto_completion = new AutoCompletion(mockFetchTerms);

    // lazy -> not called yet
    expect(mockFetchTerms.mock.calls).toHaveLength(0);

    // quickly call this async function 3 times
    var vals = [];
    vals[0] = auto_completion.getSuggestions(false, "one two", "t");
    vals[1] = auto_completion.getSuggestions(false, "one two", "tw");
    vals[2] = auto_completion.getSuggestions(false, "one two", "two");

    // ... only called once!
    expect(mockFetchTerms.mock.calls).toHaveLength(1);

    expect((await vals[0]).map((t) => t.original)).toStrictEqual([
      "t-term",
      "tw-term",
      "two-term",
    ]);
    expect((await vals[1]).map((t) => t.original)).toStrictEqual([
      "tw-term",
      "two-term",
    ]);
    expect((await vals[2]).map((t) => t.original)).toStrictEqual(["two-term"]);

    expect(mockFetchTerms.mock.calls).toHaveLength(1);
  });
});
