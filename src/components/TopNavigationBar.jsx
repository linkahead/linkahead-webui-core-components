import React from "react";
import { useRef, useCallback } from "react";
import { Branding } from "../Branding";
import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";
import { UserComponent } from "./UserComponent";
import { QueryPanel, toggleQueryPanel } from "./QueryPanel";

class NavigationManagerImpl {
  constructor({ items, brand, user_component }) {
    this.items = items || [];
    this.brand = brand || Branding.name;
    this.update_listeners = [];
    this.user_component = user_component;
  }
  registerUpdateListener(listener) {
    this.update_listeners.push(listener);
  }
  addLink(link) {
    this.items.push(link);
    this.notifyUpdateListeners();
  }
  notifyUpdateListeners() {
    this.update_listeners.forEach((listener) => listener());
  }
}

export const NavigationManager = new NavigationManagerImpl({
  items: [
    <Nav.Link key="0" href="#action1">
      Home
    </Nav.Link>,
    <Nav.Link key="1" href="#action2">
      Link
    </Nav.Link>,
    <NavDropdown key="2" title="Link">
      <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
      <NavDropdown.Item href="#action4">Another action</NavDropdown.Item>
      <NavDropdown.Divider />
      <NavDropdown.Item href="#action5">Something else here</NavDropdown.Item>
    </NavDropdown>,
    <Nav.Link key="3" href="#" disabled>
      Link
    </Nav.Link>,
  ],
  user_component: <UserComponent />,
});

export const TopNavigationBar = () => {
  const panelRef = useRef();
  const scrollHandler = useCallback(() => {
    toggleQueryPanel(panelRef.current);
  }, [panelRef]);

  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <span className="d-lg-none d-inline">{NavigationManager.brand}</span>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Navbar.Brand className="d-none d-lg-inline">
            {NavigationManager.brand}
          </Navbar.Brand>
          <Nav className="me-auto my-2 my-lg-0">
            {NavigationManager.items.map((item, key) => (
              <span key={key}>{item}</span>
            ))}
          </Nav>
          <Nav ref={panelRef} className="caosdb-f-query-panel me-lg-2 full">
            <QueryPanel restore={true} scrollHandler={scrollHandler} />
          </Nav>
          <Nav>{NavigationManager.user_component}</Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};
