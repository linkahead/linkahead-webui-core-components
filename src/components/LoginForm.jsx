import React, { useState, useMemo, useCallback } from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { InfoService } from "../InfoService";
import { Message } from "./Message";
import { LoadingInfo } from "./LoadingInfo";

async function _login(username, password) {
  const service = new InfoService();
  return await service.login(username, password);
}

function handleSubmit(showLoadingInfo, then, onError) {
  return (event) => {
    event.preventDefault();
    showLoadingInfo();
    const username = event.target.username.value;
    const password = event.target.password.value;

    // deactive login form (no retry until the server answer is there or a
    // timeout).
    event.target.submit.disabled = true;

    var login_promise = _login(username, password).catch((err) => {
      // active login again
      event.target.submit.disabled = false;
      throw err;
    });

    if (then) {
      login_promise = login_promise.then(then);
    }

    if (onError) {
      login_promise.catch(onError);
    }
  };
}

class Timeout {
  constructor(cb, ms) {
    this.cb = cb;
    ms = Number.isInteger(ms) ? ms : 10000;
    this.timeout = setTimeout(this.cb, ms);
  }
  reset(ms) {
    this.pause();
    if (this.cb) {
      ms = Number.isInteger(ms) ? ms : 25000;
      this.timeout = setTimeout(this.cb, ms);
    }
  }
  pause() {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = undefined;
    }
  }
  stop() {
    this.pause();
    this.cb = undefined;
  }
}

const LoginFormInputs = ({ then, onError, showMessages, showLoadingInfo }) => {
  const { t } = useTranslation();
  const timeout = useMemo(() => new Timeout(showMessages), [showMessages]);
  const onFocus = () => timeout.pause();
  const onBlur = () => timeout.reset();
  const onClick = () => timeout.stop();

  const wrappedOnError = useCallback(
    async (error) => {
      showMessages();
      if (onError) {
        return await onError(error);
      }
    },
    [onError, showMessages],
  );

  return (
    <form
      className="m-0"
      onSubmit={handleSubmit(showLoadingInfo, then, wrappedOnError)}
    >
      <div className="d-flex flex-wrap column-gap-3">
        <div className="flex-grow-1">
          <input
            onFocus={onFocus}
            onBlur={onBlur}
            name="username"
            className="form-control"
            placeholder={t("Username")}
            aria-label={t("Username")}
            title={t("Insert your username")}
          />
        </div>
        <div className="flex-grow-1">
          <input
            onFocus={onFocus}
            onBlur={onBlur}
            name="password"
            className="form-control"
            type="password"
            placeholder={t("Password")}
            aria-label={t("Password")}
            title={t("Insert your password")}
          />
        </div>
        <div className="flex-grow-1">
          <button
            onFocus={onFocus}
            onClick={onClick}
            className="w-100 btn btn-secondary navbar-btn d-inline-block"
            name="submit"
            type="submit"
            aria-label={t("Login")}
            title={t("Click to log in.")}
          >
            {t("Login")}
          </button>
        </div>
      </div>
    </form>
  );
};

LoginFormInputs.propTypes = {
  then: PropTypes.func.isRequired,
  onError: PropTypes.func,
  showMessages: PropTypes.func.isRequired,
  showLoadingInfo: PropTypes.func.isRequired,
};

const LoginFormMessage = ({ message, messageType, showInputs }) => {
  const { t } = useTranslation();
  message = message ? (
    <div className="flex-grow-1">
      <Message type={messageType}>{message}</Message>
    </div>
  ) : undefined;

  return (
    <div className="d-flex flex-wrap column-gap-3">
      {message}
      <div className="flex-grow-1">
        <button
          className="btn btn-secondary navbar-btn d-inline-block"
          name="show"
          type="button"
          onClick={showInputs}
          aria-label={t("Show login form")}
          title={t("Show login form")}
        >
          {t("Login")}
        </button>
      </div>
    </div>
  );
};

LoginFormMessage.propTypes = {
  message: PropTypes.node,
  messageType: PropTypes.oneOf(["error", "warning", "success", "info"]),
  showInputs: PropTypes.func.isRequired,
};

export const LoginForm = ({ then, onError, message, messageType }) => {
  const [showInputs, setShowInputs] = useState("messages");
  const showMessages = useCallback(
    () => setShowInputs("messages"),
    [setShowInputs],
  );
  const showLoadingInfo = useCallback(
    () => setShowInputs("loading"),
    [setShowInputs],
  );

  if (showInputs === "inputs") {
    return (
      <LoginFormInputs
        then={then}
        onError={onError}
        showMessages={showMessages}
        showLoadingInfo={showLoadingInfo}
      />
    );
  } else if (showInputs === "loading") {
    return <LoadingInfo size="sm" />;
  }

  return (
    <LoginFormMessage
      message={message}
      messageType={messageType}
      showInputs={() => setShowInputs("inputs")}
    />
  );
};

LoginForm.propTypes = {
  then: PropTypes.func.isRequired,
  onError: PropTypes.func,
  message: PropTypes.node,
  messageType: PropTypes.oneOf(["error", "warning", "success", "info"]),
};
