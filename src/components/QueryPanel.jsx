import React from "react";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import PropTypes from "prop-types";
import { QueryForm } from "./QueryForm";

let lastQueryFormHeight = 0;
let scrollDownTimeout = undefined;

const toggleQueryPanel = (container) => {
  if (typeof scrollDownTimeout === "number") {
    clearTimeout(scrollDownTimeout);
  }
  const { top, bottom } = container.getBoundingClientRect();
  if (lastQueryFormHeight !== bottom - top) {
    // Only the height of the form has changed. This is not actually a scroll event.
    lastQueryFormHeight = bottom - top;
    return;
  }

  scrollDownTimeout = setTimeout(() => {
    scrollDownTimeout = undefined;
    if (window.pageYOffset > lastQueryFormHeight) {
      container.classList.add("condensed");
    } else {
      container.classList.remove("condensed");
    }
  }, 200);
};

const QueryPanel = (props) => {
  useEffect(() => {
    window.addEventListener("scroll", props.scrollHandler);
    return () => {
      window.removeEventListener("scroll", props.scrollHandler);
    };
  }, [props.scrollHandler]);

  return (
    <Container>
      <QueryForm {...props} />
    </Container>
  );
};

QueryPanel.propTypes = {
  scrollHandler: PropTypes.func,
};

export { QueryPanel, toggleQueryPanel };
