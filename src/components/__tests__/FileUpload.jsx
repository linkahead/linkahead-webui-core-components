import React from "react";
import { act, render } from "@testing-library/react";
import { FileUpload } from "../FileUpload";

describe("FileUpload", () => {
  test("FileUpload basic structure", async () => {
    const recordTypes = [{ value: "RT1" }, { value: "RT2" }, { value: "RT3" }];
    const { container } = await act(
      async () => await render(<FileUpload recordTypes={recordTypes} />),
    );

    const fileInput = container.querySelector("input[type='file']");
    expect(fileInput).toBeInTheDocument();

    const rTSelect = container.querySelector(
      ".caosdb-f-file-upload-recordtype-select",
    );
    expect(rTSelect).toBeInTheDocument();

    const dirInput = container.querySelector(
      ".caosdb-f-file-upload-directory-input",
    );
    expect(dirInput).toBeInTheDocument();
  });
});
