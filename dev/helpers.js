import React from "react";
import ReactDOM from "react-dom/client";

export function mockComponent(description, component, args) {
  const outerDomContainer = document.querySelector("#root");

  const domContainer = document.createElement("div");
  domContainer.className = "mock-container";
  domContainer.setAttribute("style", "margin-bottom: 5px");
  outerDomContainer.appendChild(domContainer);

  const domContainerBody = document.createElement("div");
  domContainerBody.className = "mock-container-body";
  domContainerBody.setAttribute(
    "style",
    "display: inline-block; border: 2px solid black;",
  );
  domContainer.appendChild(domContainerBody);

  const domContainerDescription = document.createElement("div");
  domContainerDescription.className = "mock-container-description";
  domContainerDescription.setAttribute(
    "style",
    "background-color: black; color: white;",
  );
  domContainerDescription.innerHTML = description;
  domContainer.appendChild(domContainerDescription);

  const root = ReactDOM.createRoot(domContainerBody);
  root.render(React.createElement(component, args));
}
