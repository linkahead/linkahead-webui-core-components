/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2019 Henrik tom Wörden <h.tomwoerden@indiscale.com>
 * Copyright (C) 2019-2023 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2019-2023 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/////// AUTO COMPLETION
const CQL_WORDS = [
  "FIND",
  "FILE",
  "ENTITY",
  "SELECT",
  "COUNT",
  "RECORD",
  "PROPERTY",
  "RECORDTYPE",
  "REFERENCES",
  "REFERENCED BY",
  "WHICH",
  "WITH",
  "CREATED BY",
  "CREATED BY ME",
  "CREATED AT",
  "CREATED ON",
  "CREATED IN",
  "CREATED BEFORE",
  "CREATED UNTIL",
  "CREATED AFTER",
  "CREATED SINCE",
  "SOMEONE",
  "STORED AT",
  "HAS A PROPERTY",
  "HAS BEEN",
  "ANY VERSION OF",
  "FROM",
  "INSERTED AT",
  "INSERTED ON",
  "INSERTED IN",
  "INSERTED BY",
  "INSERTED BY ME",
  "INSERTED BEFORE",
  "INSERTED UNTIL",
  "INSERTED AFTER",
  "INSERTED SINCE",
  "UPDATED AT",
  "UPDATED ON",
  "UPDATED IN",
  "UPDATED BY",
  "UPDATED BY ME",
  "UPDATED BEFORE",
  "UPDATED UNTIL",
  "UPDATED AFTER",
  "UPDATED SINCE",
  "SINCE",
  "BEFORE",
  "ON",
  "IN",
  "AFTER",
  "UNTIL",
  "AT",
  "BY",
  "BY ME",
];

function prepareTerms(terms) {
  return terms.map((term) => {
    var term_escaped = term;
    if (term_escaped.indexOf(" ") > -1) {
      if (term.indexOf('"') > -1) {
        term_escaped = `'${term_escaped}'`;
      } else {
        term_escaped = `"${term_escaped}"`;
      }
    }
    return {
      original: term,
      normalized: term.trim().toLowerCase(),
      escaped: term_escaped,
    };
  });
}

function prepareCqlWords(words) {
  return words.map((word) => ({
    original: word,
    normalized: word.trim().toLowerCase(),
    escaped: word,
  }));
}

export class AutoCompletion {
  constructor(terms, cql_words) {
    this._cql_words = prepareCqlWords(cql_words || CQL_WORDS);
    this._terms = terms || [];
    this._resolved_terms = undefined;
  }

  async getSuggestions(isCql, fullQuery, word) {
    if (isCql) {
      return await this.getCqlSuggestions(fullQuery, word);
    }
    return await this.getSearchTermSuggestions(fullQuery, word);
  }

  async _do_resolve_terms(terms) {
    var resolved_terms;
    if (typeof terms === "function") {
      resolved_terms = await terms();
    } else {
      resolved_terms = await terms;
    }
    return await prepareTerms(resolved_terms);
  }

  async _getTerms() {
    if (!this._resolved_terms) {
      this._resolved_terms = this._do_resolve_terms(this._terms);
    }
    return await this._resolved_terms;
  }

  async getSearchTermSuggestions(fullQuery, word) {
    var terms = await this._getTerms();
    if (fullQuery === word) {
      // still only one word -> also append FIND, SELECT and COUNT
      terms = prepareCqlWords(["FIND", "SELECT", "COUNT"]).concat(terms);
    }
    const wordNormalized = word.trim().toLowerCase();
    return terms.filter((term) => term.normalized.startsWith(wordNormalized));
  }

  async getCqlSuggestions(fullQuery, word) {
    const cql_words = this._cql_words;

    const wordNormalized = word.trim().toLowerCase();
    return cql_words
      .filter((term) => term.normalized.startsWith(wordNormalized))
      .concat(await this.getSearchTermSuggestions(fullQuery, word));
  }
}

/////// QUERY GENERATION

export const isCql = function (query) {
  query = query.toUpperCase().trim();
  return (
    query.startsWith("FIND") ||
    query.startsWith("COUNT") ||
    query.startsWith("SELECT")
  );
};

export const isSelect = function (query) {
  return query.toUpperCase().trim().startsWith("SELECT");
};

const translator = {
  "=": "=",
  ">": ">",
  "≥": ">=",
  "<": "<",
  "≤": "<=",
  "≠": "!=",
  equals: "=",
  in: "in",
  "not in": "not in",
  "after date": ">",
  "since date": ">=",
  "until date": "<=",
  "before date": "<",
  "on date": "on",
  "not on date": "not on",
};

/**
 * Convert a (property, operator, value, unit) tuple to a valid CQL POV-filter
 * expression including quotation for the value and parenthesis around
 * everything for safe interpretation by the CQL parser.
 *
 *   E.g. ("Event.longitude", ">", -40, "°") results in
 *        '(Event.longitude > "-40°")'.
 *
 * Note: The correct quotation for the property must be included in the passed
 * `property` already and hence be defined in the QueryForm~FilterDefinition.
 *
 * Note: This is needed by makeQuery and handles the most basic cases. For
 * operators like `not contains` or `range` there are special treatments
 * elsewhere.
 *
 * @param {string} property
 * @param {string} operator
 * @param {string|number} value
 * @param {string} [unit]
 * @return {string} CQL POV-filter expression.
 */
const toCQL = (property, operator, value, unit) => {
  value = value || "";
  unit = unit || "";
  const quotes = unit !== "" ? "" : value.indexOf('"') === -1 ? '"' : "'";
  if (translator[operator]) {
    return `(${property} ${translator[operator]} ${quotes}${value}${unit}${quotes})`;
  }
  throw new Error(`Could not translate ${property} ${operator} filter`, value);
};

export const allowed_operators = {
  boolean: ["is true", "is false"],
  text: ["equals", "not equals", "contains", "not contains"],
  integer: ["=", "≠", "≥", "≤", "<", ">", "range"],
  double: ["=", "≠", "≥", "≤", "<", ">", "range"],
  datetime: [
    "=",
    "≠",
    "on date",
    "in",
    "not on date",
    "not in",
    "after date",
    "before date",
    "until date",
    "since date",
  ],
};

/**
 * Dictionary of functions:
 *
 * (datatype,operator) -> function(property, operator, value, unit)
 */
const make_query_filter = {
  boolean: {
    "is true": (property) => `(${property} = TRUE)`,
    "is false": (property) => `(${property} = FALSE)`,
  },
  text: {
    contains: (property, operator, value) => `(${property} LIKE "*${value}*")`,
    "not equals": (property, operator, value) =>
      `(NOT ${property} = "${value}")`,
    "not contains": (property, operator, value) =>
      `(NOT ${property} LIKE "*${value}*")`,
  },
  integer: {
    range: (property, operator, value, unit) =>
      `(${property} > ${value[0]}${unit}) AND (${property} < ${value[1]}${unit})`,
  },
  double: {
    range: (property, operator, value, unit) =>
      `(${property} > ${value[0]}${unit}) AND (${property} < ${value[1]}${unit})`,
  },
  _default: { _default: toCQL },
};

/**
 * Generate a valid CQL POV-filter expressions appropriate for a tuple
 * (datatype, property, operator, value, unit).
 *
 * @param {string} datatype
 * @param {string} property
 * @param {string} operator
 * @param {string|number} value
 * @param {string} [unit]
 * @return {string} CQL POV-filter expression.
 *
 */
export const makeQueryFilter = (datatype, property, operator, value, unit) => {
  if (!datatype || !operator) {
    return undefined;
  }
  unit = unit || "";

  if (make_query_filter[datatype]) {
    if (
      make_query_filter[datatype][operator] ||
      make_query_filter[datatype][operator] === null
    ) {
      return make_query_filter[datatype][operator](
        property,
        operator,
        value,
        unit,
      );
    } else if (
      make_query_filter[datatype]["_default"] ||
      make_query_filter[datatype]["_default"] === null
    ) {
      return make_query_filter[datatype]["_default"](
        property,
        operator,
        value,
        unit,
      );
    }
  }

  if (make_query_filter["_default"][operator]) {
    return make_query_filter["_default"][operator](
      property,
      operator,
      value,
      unit,
    );
  }

  return make_query_filter["_default"]["_default"](
    property,
    operator,
    value,
    unit,
  );
};

/**
 * @param {string} record_type
 * @returns {QueryForm~makeQueryCallback}
 */
export const makeQueryTemplate =
  (record_type) => (search_terms, activeFilters) => {
    var query = "FIND RECORD";
    if (record_type) {
      query += ` "${record_type}"`;
    }
    var filter_connector = " WITH ";
    if (search_terms && search_terms.length > 0) {
      const splitted_search_terms = splitSearchTerms(search_terms).map(
        (word) => `A PROPERTY LIKE '*${word.replaceAll("'", `\\'`)}*'`,
      );
      if (splitted_search_terms.length) {
        filter_connector = " AND ";
        query += " WITH " + splitted_search_terms.join(filter_connector);
      }
    }

    const filters = activeFilters.map((filter) =>
      makeQueryFilter(
        filter.datatype,
        filter.property,
        filter.operator,
        filter.value,
        filter.unit,
      ),
    );

    if (filters.length > 0) {
      query += filter_connector + filters.join(" AND ");
    }

    return query;
  };

const _splitSearchTermsPattern =
  /"(?<dq>[^"]*)" |'(?<sq>[^']*)' |(?<nq>[^ ]+)/g;

/**
 * Split a query string into single terms.
 *
 * Terms are separated by white spaces. Terms which contain white spaces
 * which are to be preserved must be enclosed in " or ' quotes. The
 * enclosing quotation marks are being stripped. Currently no support for
 * escape sequences for quotation marks.
 *
 * @function splitSearchTerms
 * @param {string} query - complete query string.
 * @return {string[]} array of the search terms.
 */
const splitSearchTerms = function (query) {
  // add empty space at the end, so every matching group ends with it -> easier regex. Also, undefined is filtered out
  return Array.from(
    (query + " ").matchAll(_splitSearchTermsPattern),
    (m) => m[1] || m[2] || m[3],
  ).filter((word) => word);
};

////// TABS

/**
 * FilterDefinitions define which filters can be selected by the users.
 *
 * @typedef {Object} QueryForm~FilterDefinition
 * @public
 * @property {string} property
 * @property {string} datatype
 * @property {number} [min]
 * @property {number} [max]
 * @property {number} [step]
 * @property {strint} [unit]
 * @property {string} [inputType]
 */

/**
 * QueryFilters specify the current filters a user has specified using the
 * QueryForm.
 *
 * @typedef {Object} QueryForm~QueryFilter
 * @public
 * @property {string} property
 * @property {string} datatype
 * @property {stirng} operator
 * @property {string} unit
 * @property {string|number} value
 */

/**
 * When the query form is being submitted, this callback generates the query
 * from the search terms and active Filters.
 *
 * @callback QueryForm~makeQueryCallback
 * @public
 * @param {string} searchTerms
 * @param {QueryForm~QueryFilter[]} activeFilters
 * @returns {string} The generated CQL query.
 */

/**
 * Create a tab object.
 *
 * @function
 * @public
 * @param {string} id
 * @param {string} recordType
 * @param {string} [label]
 * @param {string} [description]
 * @param {QueryForm~makeQueryCallback} [makeQueryCallback=makeQueryTemplate(id)]
 * @param {QueryForm~FilterDefinition[]} [filterDefinitions=[]]
 */
export function createTab(
  id,
  recordType,
  label,
  description,
  makeQueryCallback,
  filterDefinitions,
) {
  return {
    id: id,
    recordType: recordType,
    label: label || recordType || id,
    description:
      description || `Show only results of type ${label || recordType || id}`,
    makeQuery: makeQueryCallback || makeQueryTemplate(recordType),
    filterDefinitions: filterDefinitions || [],
  };
}

////// DEBUGGING AND TESTING

/**
 * Create some query panel tabs for testing and debugging.
 */
export const get_test_tabs = () => {
  const makeFilterDefinitions = (id) => [
    { property: id + "-text-property", datatype: "text" },
    { property: id + "-datetime-property", datatype: "datetime" },
    { property: id + "-integer-property", datatype: "integer" },
    {
      property: id + "-double-property",
      datatype: "double",
      min: "-50",
      max: "50",
      unit: "m",
    },
    { property: id + "-boolean-property", datatype: "boolean" },
    {
      property: `"${id}-reference-property"."sub-property"`,
      label: `sub-property of ${id}-reference-property`,
      datatype: "text",
    },
  ];
  return [
    createTab(
      "all",
      undefined,
      "All",
      "Show all matching results",
      makeQueryTemplate(""),
      makeFilterDefinitions("all"),
    ),
    createTab(
      "dataset",
      "Dataset",
      "Dataset",
      undefined,
      makeQueryTemplate("Dataset"),
      makeFilterDefinitions("dataset"),
    ),
    createTab(
      "event",
      "Event",
      "Event",
      undefined,
      makeQueryTemplate("Event"),
      makeFilterDefinitions("event"),
    ),
    createTab(
      "person",
      "Person",
      "Person",
      undefined,
      makeQueryTemplate("Person"),
      makeFilterDefinitions("person"),
    ),
  ];
};

export const get_test_suggestions = async (isCql, full, prefix) => {
  const ac = new AutoCompletion(
    () => [
      prefix + "-suggestion1",
      prefix + "-suggestion2",
      prefix + "-suggestion3",
      prefix + "-suggestion4",
      prefix + "-suggestion5",
      prefix + "-suggestion6",
      prefix + "-suggestion7",
      prefix + "-suggestion8",
      prefix + "-suggestion9",
      prefix + "-suggestion10",
      prefix + "-suggestion11",
      prefix + "-suggestion12",
      prefix + "-suggestion13",
      prefix + "-suggestion14",
      prefix + "-suggestion15",
    ],
    CQL_WORDS,
  );
  return ac.getSuggestions(isCql, full, prefix);
};
