import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import { reducer, QueryForm } from "../QueryForm";
import { createTab } from "../QueryForm.helpers";

test("QueryForm is available", () => {
  const tabs = [createTab("tab-id")];
  const { container } = render(<QueryForm tabs={tabs} />);
  const formElement = container.firstChild;
  expect(formElement).toBeInTheDocument();
  expect(formElement).toMatchSnapshot();
});

describe("Test basic flows", () => {
  test("Type into search input and submit", () => {
    const submitCallback = jest.fn((x) => x);
    render(
      <QueryForm
        submitCallback={submitCallback}
        getSuggestionsCallback={false}
      />,
    );

    expect(screen.queryByTitle("Search")).toBeInTheDocument();
    expect(screen.queryByTitle("Search").disabled).toBe(true);
    expect(
      screen.queryByTitle("Enter search terms").parentNode,
    ).toMatchSnapshot();

    fireEvent.change(screen.getByTitle("Enter search terms"), {
      target: { value: "a" },
    });

    expect(screen.queryByTitle("Search")).toBeInTheDocument();
    expect(
      screen.queryByTitle("Enter search terms").parentNode,
    ).toMatchSnapshot();

    fireEvent.change(screen.getByTitle("Enter search terms"), {
      target: { value: "" },
    });
    expect(screen.queryByTitle("Search")).toBeInTheDocument();
    expect(screen.queryByTitle("Search").disabled).toBe(true);

    fireEvent.change(screen.getByTitle("Enter search terms"), {
      target: { value: "random_search_term" },
    });

    expect(submitCallback.mock.calls).toHaveLength(0);
    fireEvent.click(screen.getByTitle("Search"));
    expect(submitCallback.mock.calls).toHaveLength(1);
    expect(submitCallback.mock.calls[0][0]).toMatch(/random_search_term/);
  });

  test("Choose filter", () => {
    const tabs = [
      createTab("tab-id", undefined, undefined, "Show tab-id", undefined, [
        // filterDefinitions
        { property: "some-prop", datatype: "text" },
      ]),
    ];
    const submitCallback = jest.fn((x) => x);
    const { container } = render(
      <QueryForm
        submitCallback={submitCallback}
        tabs={tabs}
        getSuggestionsCallback={false}
      />,
    );

    expect(screen.getByTitle("Show tab-id")).toBeInTheDocument();
    expect(screen.getByTitle("Show tab-id").className).toMatch(/active/);
    expect(screen.getByTitle("Show tab-id").tabIndex).toBe(-1);
    expect(screen.getByTitle("Advanced Filtering")).toBeInTheDocument();

    expect(screen.getByTitle("Show tab-id").parentNode).toMatchSnapshot();

    // click "Advanced Filtering"
    expect(screen.queryByTitle("Add Filter")).not.toBeInTheDocument();
    fireEvent.click(screen.getByTitle("Advanced Filtering"));
    expect(screen.getByTitle("Add Filter")).toBeInTheDocument();
    expect(
      container.getElementsByClassName("caosdb-f-query-tab-filters")[0],
    ).toMatchSnapshot();

    // Choose filter
    fireEvent.change(screen.getByTitle("Add Filter"), {
      target: { value: "some-prop" },
    });
  });
});

describe("caosdb-webui#224 infinite loop", () => {
  test("dispatch onChange", () => {
    const getSuggestionsCallback = jest.fn(() => "called");
    var state = {};
    var action = {
      target: {
        selectionEnd: 3,
        value: "FIN",
      },
      getSuggestionsCallback: getSuggestionsCallback,
      type: "onChange",
    };
    // set changeId
    expect(reducer(state, action)).toMatchObject({
      value: "FIN",
      changeId: 0,
      unresolvedSuggestions: "called",
    });
    // fetching suggestion has been triggered
    expect(getSuggestionsCallback).toHaveBeenCalledTimes(1);

    state.changeId = 1;
    // increase changeId
    expect(reducer(state, action)).toMatchObject({
      value: "FIN",
      changeId: 2,
      unresolvedSuggestions: "called",
    });
    // fetching suggestion has been triggered
    expect(getSuggestionsCallback).toHaveBeenCalledTimes(2);
  });

  test("dispatch resolveSuggestions", () => {
    var state = {
      changeId: 5,
      unresolvedSuggestions: () => ["FIND"],
    };
    var action = {
      changeId: 4,
      suggestions: ["FIND"],
      type: "resolveSuggestions",
    };

    // ignored, changeId does not match -> no state change
    expect(reducer(state, action)).toMatchObject(state);

    action.changeId = 5;
    // changeId matches, suggestions are being inserted
    expect(reducer(state, action)).toMatchObject({
      ...state,
      unresolvedSuggestions: undefined,
      suggestions: action.suggestions,
    });
  });
});
