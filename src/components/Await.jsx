/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";
import { LoadingInfo } from "./LoadingInfo";
import { useState, useEffect } from "react";

/**
 * Wrapper for handling Promises. The results of the promise are being
 * rendered. In the mean time, a LoadingInfo is being shown.
 *
 * Example:
 *   <Await promise={fetchData()}
 *          then={data => createTable(data)}
 *          catch={error => "<div>Error: " + error.message</div>}/>
 *
 * When fetchData resolves, the data is being presented as a table. An error
 * div is being shown if fetchData fails.
 *
 * NB. Equivalently, you could also do:
 *   <Await promise={fetchData()
 *          .then(data => createTable(data)
 *          .catch=(error => "<div>Error: " + error.message</div>)
 *   }/>
 *
 * @param {Promise} promise - Must resolve to a React.Component unless the
 * parameter `then` does the convesion.
 * @param {function} [then] - function with one parameter. It is being called
 * when the promise resolves. It should preprocess the promises' results and
 * must a component.
 * @param {function} [catch] - function with one parameter. It is being called
 * when the promise rejects. It should preprocess the error and must return a
 * component.
 * @param {React.Component} [loading] - A component which will show until the
 * promise resolves. If undefined, nothing will be shown.
 */

export function Await(props) {
  const loading = props.loading || <LoadingInfo />;
  const [state, setState] = useState(loading);

  useEffect(() => {
    const handle = async (thePromise, doThen, doCatch) => {
      var result = undefined;
      try {
        result = await thePromise;
        if (doThen) {
          result = await doThen(result);
        }
      } catch (error) {
        if (doCatch) {
          result = await doCatch(error);
        } else {
          const _error = error && error.message ? error.message : error;
          result = <span>An unhandled error occured: {_error}</span>;
        }
      }
      setState(result);
    };

    handle(props.promise, props.then, props.catch);
  }, [props.promise, props.then, props.catch, setState]);

  return state;
}
