# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

* Componets `UserComponent` and `LoginForm` support internationalization with
  languages "en" and "de" now.
* FileUpload component
  * Select RecordType from list.
  * Create new directories.
* QueryForm component
  * Tabs for quickly selecting the record type of the results.
  * Query Filters for generating queries by selecting properties and operators
    from drop-down menus.
* "onLogin" and "onLogout" call-back functions for the UserComponent component
* Click handler for the query bar's auto-completion drop-down.

### Changed

* Removed background-color of body from the style

### Deprecated

### Removed

### Fixed

* Infinite loop in query panel auto-completion.
  [caosdb-webui#225](https://gitlab.com/caosdb/caosdb-webui/-/issues/225)
* Missing `autocomplete="off"` in query form.
  [caosdb-webui#223](https://gitlab.com/caosdb/caosdb-webui/-/issues/223)
* AutoCompletion class calls the fetch/load terms functions more than once.
* Styling of the query filters (even columns where all filters are aligned horizontally).
* UTF8 operator symbols for the query filters instead of ascii based ones (e.g.
  `≥` instead of `>=`).
* [IndiScale#8](https://gitlab.indiscale.com/caosdb/src/caosdb-webui-core-components/-/issues/8)
  files-ui-react dependency would load fonts from google.com.

### Security
